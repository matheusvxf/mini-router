# cse-123-project-2
Mini-Router with Network Address Translation. Protocols tested: MAC (Ethernet), ARP, ICMP (ping and traceroute), IP, UDP, TCP and HTTP

All the tests were done with simulations using Mininet: http://mininet.org/
