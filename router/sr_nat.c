/*
 * sr_nat.c
 *
 *  Created on: Dec 11, 2014
 *      Author: matheus
 */

#include "sr_nat.h"
#include "sr_utils.h"

#include <stdlib.h>
#include <string.h>

char *private_interface_str[] = {"10.0.0.0", "172.16.0.0", "192.168.0.0"};
char *private_interface_msk_str[] = {"255.0.0.0", "255.240.0.0", "255.255.0.0"};
struct in_addr private_interface_ip[3];
struct in_addr private_interface_msk[3];

void sr_nat_init(struct sr_instance *sr, sr_nat_table_t *table){
	uint32_t i;

	memset(table, 0, sizeof(sr_nat_table_t));

	for(i = 0; i < 3; ++i){
		inet_aton(private_interface_str[i], &private_interface_ip[i]);
		inet_aton(private_interface_msk_str[i], &private_interface_msk[i]);
	}

	for(i = 0; i < 3; ++i){
		table->private_ip[i] = private_interface_ip[i].s_addr;
	}
}

uint32_t sr_nat_is_private_addr(sr_nat_table_t *table, uint32_t ip_addr){
	uint32_t i;

	for(i = 0; i < 3; ++i)
		if(table->private_ip[i] == (ip_addr & private_interface_msk[i].s_addr))
			return TRUE;

	return FALSE;
}

sr_nat_t *sr_nat_lookup(sr_nat_table_t *table, uint32_t ip_addr, uint16_t port)
{
	sr_nat_t *nat_walker = table->first;

	while(nat_walker != NULL){
		if((nat_walker->lan_side == ip_addr && nat_walker->lan_port == port) ||
				(nat_walker->wan_side == ip_addr && nat_walker->wan_port == port)){
			return nat_walker;
		}

		nat_walker = nat_walker->next;
	}

	return NULL;
}

sr_nat_t *sr_nat_insert_port(sr_nat_table_t *table, uint32_t lan_side, uint16_t lan_port, uint32_t wan_side, uint32_t wan_port)
{
	sr_nat_t *nat_walker = table->first;
	sr_nat_t *tmp = nat_walker;

	while(nat_walker != NULL){
		if(nat_walker->lan_side == lan_side && nat_walker->lan_port == lan_port) { /* already inserted */
			return nat_walker;
		}

		tmp = nat_walker;
		nat_walker = nat_walker->next;
	}

	if(table->first == NULL){
		table->first = (sr_nat_t*)malloc(sizeof(sr_nat_t));
		memset(table->first, 0, sizeof(sr_nat_t));
		tmp = table->first;
	}
	else {
		tmp->next = (sr_nat_t*)malloc(sizeof(sr_nat_t));
		memset(tmp->next, 0, sizeof(sr_nat_t));
		tmp = tmp->next;
	}

	tmp->lan_side = lan_side;
	tmp->lan_port = lan_port;
	tmp->wan_side = wan_side;
	tmp->wan_port = wan_port;

	return tmp;
}

sr_nat_t *sr_nat_insert(sr_nat_table_t *table, uint32_t lan_side, uint16_t lan_port, uint32_t wan_side)
{
	return sr_nat_insert_port(table, lan_side, lan_port, wan_side, rand() % (65535 - 1024) + 1024);
}

void sr_nat_map_incoming_packet(sr_instance_t *sr, sr_ethernet_hdr_t *packet){
	sr_ip_hdr_t *ip_packet, *icmp_ip_packet;
	sr_transport_t *transport_packet;
	sr_icmp_t3_hdr_t *icmp_packet;
	sr_tcp_hdr_t *tcp_packet;
	sr_udp_hdr_t *udp_packet;
	sr_nat_t *nat_entry_dst;
	uint32_t is_src_priv;
	uint8_t protocol;

	if(packet->ether_type != htons(ethertype_ip)) /* Just map IP packets */
		return;

	ip_packet = ip_hdr(packet);
	protocol = ip_packet->ip_p;

	is_src_priv = sr_nat_is_private_addr(sr->nat, ip_packet->ip_src);

	if(is_src_priv) /* We handle just packets going from the public network to the private network here */
		return;

	/* Recover transport layer information */
	switch(protocol){
	case ip_protocol_tcp:
	case ip_protocol_udp:
		transport_packet = (sr_transport_t*)sr_ip_upper_layer(ip_packet);

		nat_entry_dst = sr_nat_lookup(sr->nat, ip_packet->ip_dst, transport_packet->dst_port);
		ip_packet->ip_dst = nat_entry_dst->lan_side;
		transport_packet->dst_port = nat_entry_dst->lan_port;
		break;
	case ip_protocol_icmp:
		icmp_packet = (sr_icmp_t3_hdr_t*)sr_ip_upper_layer(ip_packet);
		icmp_ip_packet = (sr_ip_hdr_t*)sr_icmp_data(icmp_packet);
		transport_packet = (sr_transport_t*)sr_ip_upper_layer(icmp_ip_packet);

		switch(icmp_packet->icmp_type){
		case icmp_echo_reply:
			nat_entry_dst = sr_nat_lookup(sr->nat, ip_packet->ip_dst, icmp_packet->unused);
			break;
		case icmp_destination_unreachable:
			nat_entry_dst = sr_nat_lookup(sr->nat, ip_packet->ip_dst, transport_packet->src_port);
			icmp_ip_packet->ip_src = nat_entry_dst->lan_side;
			transport_packet->src_port = nat_entry_dst->lan_port;

			break;
		default:
			return;
		}
		ip_packet->ip_dst = nat_entry_dst->lan_side;
		break;
	default:
		return;
	}

	ip_packet->ip_sum = 0;
	ip_packet->ip_sum = cksum((uint8_t*)ip_packet, ip_packet->ip_hl * 4);

	switch(ip_packet->ip_p){
	case ip_protocol_icmp:
		icmp_packet->icmp_sum = 0;
		icmp_packet->icmp_sum = cksum((uint8_t*)icmp_packet, ntohs(ip_packet->ip_len) - ip_packet->ip_hl * 4);
		break;
	case ip_protocol_udp:
		udp_packet = (sr_udp_hdr_t*)transport_packet;
		udp_packet->checksum = 0;
		udp_packet->checksum = cksum((uint8_t*)udp_packet, udp_packet->length);
		break;
	case ip_protocol_tcp:
		tcp_packet = (sr_tcp_hdr_t*)transport_packet;
		tcp_packet->checksum = 0;
		tcp_packet->checksum = cksum((uint8_t*)tcp_packet, ntohs(ip_packet->ip_len) - ip_packet->ip_hl * 4);
		break;
	}
}

void sr_nat_map_outgoing_packet(sr_instance_t *sr, sr_ethernet_hdr_t *packet){
	sr_ip_hdr_t *ip_packet;
	sr_transport_t *transport_packet;
	sr_icmp_t3_hdr_t *icmp_packet;
	sr_tcp_hdr_t *tcp_packet;
	sr_udp_hdr_t *udp_packet;
	sr_if_t *outgoing_interface;
	sr_nat_t *nat_entry_src;
	uint32_t is_src_priv, is_dst_priv;
	uint8_t protocol;

	if(packet->ether_type != htons(ethertype_ip)) /* Just map IP packets */
		return;

	ip_packet = ip_hdr(packet);
	protocol = ip_packet->ip_p;

	outgoing_interface = sr_get_interface_by_mac_addr(sr, packet->ether_shost);
	is_src_priv = sr_nat_is_private_addr(sr->nat, ip_packet->ip_src);
	is_dst_priv = sr_nat_is_private_addr(sr->nat, ip_packet->ip_dst);

	if(!is_src_priv && is_dst_priv) /* We handle just packets going from the private network to the public network here */
		return;

	/* Recover transport layer information */
	/* We need to create a NAT entry in the table */
	switch (protocol){
	case ip_protocol_tcp:
	case ip_protocol_udp:
		transport_packet = (sr_transport_t*)sr_ip_upper_layer(ip_packet);

		nat_entry_src = sr_nat_insert(sr->nat, ip_packet->ip_src, transport_packet->src_port, outgoing_interface->ip);
		ip_packet->ip_src = nat_entry_src->wan_side;
		transport_packet->src_port = nat_entry_src->wan_port;
		break;
	case ip_protocol_icmp:
		icmp_packet = (sr_icmp_t3_hdr_t*)sr_ip_upper_layer(ip_packet);
		transport_packet = (sr_transport_t*)sr_icmp_upper_layer(icmp_packet);

		switch(icmp_packet->icmp_type){
		case icmp_echo_request:
			nat_entry_src = sr_nat_insert_port(sr->nat, ip_packet->ip_src, icmp_packet->unused, outgoing_interface->ip, icmp_packet->unused);
			break;
		default:
			return;
		}
		ip_packet->ip_src = nat_entry_src->wan_side;
		break;
	default:
		return;
	}

	ip_packet->ip_sum = 0;
	ip_packet->ip_sum = cksum((uint8_t*)ip_packet, ip_packet->ip_hl * 4);

	switch(ip_packet->ip_p){
	case ip_protocol_icmp:
		icmp_packet->icmp_sum = 0;
		icmp_packet->icmp_sum = cksum((uint8_t*)icmp_packet, ntohs(ip_packet->ip_len) - ip_packet->ip_hl * 4);
		break;
	case ip_protocol_udp:
		udp_packet = (sr_udp_hdr_t*)transport_packet;
		udp_packet->checksum = 0;
		udp_packet->checksum = cksum((uint8_t*)udp_packet, ntohs(udp_packet->length));
		break;
	case ip_protocol_tcp:
		tcp_packet = (sr_tcp_hdr_t*)transport_packet;
		tcp_packet->checksum = 0;
		tcp_packet->checksum = cksum((uint8_t*)tcp_packet, ntohs(ip_packet->ip_len) - ip_packet->ip_hl * 4);
		break;
	}
}


