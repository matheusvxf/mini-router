/*
 *  Copyright (c) 2009 Roger Liao <rogliao@cs.stanford.edu>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef SR_UTILS_H
#define SR_UTILS_H

uint16_t cksum(const void *_data, int len);

uint16_t ethertype(uint8_t *buf);
uint8_t *sr_ethernet_upper_layer(sr_ethernet_hdr_t *buf);
uint8_t *sr_ip_upper_layer(sr_ip_hdr_t *buf);
uint8_t *sr_icmp_data(sr_icmp_t3_hdr_t *buf);
uint8_t *sr_icmp_upper_layer(sr_icmp_t3_hdr_t *buf);
sr_arp_hdr_t* arp_hdr(sr_ethernet_hdr_t *buf);
sr_icmp_t3_hdr_t* icmp_hdr(sr_ethernet_hdr_t *buf);
sr_ip_hdr_t* ip_hdr(sr_ethernet_hdr_t *buf);
uint16_t arp_opcode(uint8_t *buf);
uint8_t ip_protocol(uint8_t *buf);

void swap_uint32(uint32_t *a, uint32_t *b);
void swap_buffer(uint8_t *buf_1, uint8_t *buf_2, uint32_t size);
char *mac2str(uint8_t *addr);
char *ipv42str(uint32_t addr);
void ip2str(char buf[32], uint32_t addr);
void hton_mac(uint8_t dst [ETHER_ADDR_LEN], uint8_t src[ETHER_ADDR_LEN]);

void print_addr_eth(uint8_t *addr);
void print_addr_ip(struct in_addr address);
void print_addr_ip_int(uint32_t ip);

void print_hdr_eth(uint8_t *buf);
void print_hdr_ip(uint8_t *buf);
void print_hdr_icmp(uint8_t *buf);
void print_hdr_arp(uint8_t *buf);

/* prints all headers, starting from eth */
void print_hdrs(uint8_t *buf, uint32_t length);

#define TRUE 1
#define FALSE 0
#define DEBUG TRUE
#define NAT TRUE

#if (DEBUG == TRUE)
uint8_t debug_buffer[1024];
#define DEBUG_MSG(...) printf(__VA_ARGS__)
#else
#define DEBUG_MSG(msg, ...)		{}
#endif

#endif /* -- SR_UTILS_H -- */
