/*
 * sr_nat.h
 *
 *  Created on: Dec 11, 2014
 *      Author: matheus
 */

#ifndef SR_NAT_H_
#define SR_NAT_H_

#include "sr_protocol.h"
#include "sr_router.h"

#define PRIVATE_ADDR_1 12

typedef struct sr_nat {
	uint32_t lan_side;
	uint16_t lan_port;
	uint32_t wan_side;
	uint16_t wan_port;
	struct sr_nat *next;
} sr_nat_t;

typedef struct sr_nat_table {
	sr_nat_t *first;
	uint32_t private_ip[3]; /* Router private IP addresses */
} sr_nat_table_t;

void sr_nat_init(struct sr_instance *sr, sr_nat_table_t *table);
uint32_t sr_nat_is_private_addr(sr_nat_table_t *table, uint32_t ip_addr);
sr_nat_t *sr_nat_lookup(sr_nat_table_t *table, uint32_t ip_addr, uint16_t port);
sr_nat_t *sr_nat_insert(sr_nat_table_t *table, uint32_t lan_side, uint16_t lan_port, uint32_t wan_side);
void sr_nat_map_incoming_packet(sr_instance_t *sr, sr_ethernet_hdr_t *packet);
void sr_nat_map_outgoing_packet(sr_instance_t *sr, sr_ethernet_hdr_t *packet);

#endif /* SR_NAT_H_ */
