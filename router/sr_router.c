/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>


#include "sr_if.h"
#include "sr_rt.h"
#include "sr_nat.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

#include <stdlib.h>
#include <string.h>
#include <string.h>

/* Router private functions */
static void sr_handle_ip_packet(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */);
static void sr_router_handle_ip_packet(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */);
static void sr_handle_arp_packet(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */);
static void sr_handle_arp_request(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */);
static void sr_send_arp_reply(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */,
		unsigned char addr[ETHER_ADDR_LEN]);
static void sr_handle_arp_reply(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */);
static void sr_forward_ip_packet(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
		sr_if_t *interface,
		sr_arpentry_t *arp_entry);
static int sr_send_arp(sr_instance_t *sr, sr_arp_opcode_t opcode,
		uint8_t sha[ETHER_ADDR_LEN], uint32_t s_ip,
		uint8_t tha[ETHER_ADDR_LEN], uint32_t t_ip);
static int sr_send_ip(sr_instance_t *sr, sr_ethernet_hdr_t *packet, uint32_t len, uint16_t upper_layer_len, uint8_t protocol, uint32_t s_ip_addr, uint32_t d_ip_addr);
static int sr_send_ethernet(sr_instance_t *sr, sr_ethernet_hdr_t *packet, uint32_t len, sr_ethertype_t type, uint8_t ether_shost[ETHER_ADDR_LEN], uint8_t ether_dhost[ETHER_ADDR_LEN]);

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr)
{
    /* REQUIRES */
    assert(sr);

    sr->nat = (sr_nat_table_t*)malloc(sizeof(sr_nat_table_t));

    /* Initialize cache and cache cleanup thread */
    sr_arpcache_init(&(sr->cache));
    sr_nat_init(sr, sr->nat);

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
    
    /* Add initialization code here! */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

void sr_handlepacket(struct sr_instance* sr,
        uint8_t * packet/* lent */,
        unsigned int len,
        char* interface/* lent */)
{
	sr_ip_hdr_t *ip_packet = ip_hdr((sr_ethernet_hdr_t*)packet);
	uint16_t type = ethertype(packet);

	/* REQUIRES */
	assert(sr);
	assert(packet);
	assert(interface);

	printf("*** -> Received packet of length %d \n", len);

#if (DEBUG == TRUE)
	char src[32], dst[32];
	ip2str(src, ip_packet->ip_src);
	ip2str(dst, ip_packet->ip_dst);
	DEBUG_MSG("sr_handle_ip_packet src: %s dst: %s\n", src, dst);
#endif

#if (NAT == TRUE)
	sr_nat_map_incoming_packet(sr, (sr_ethernet_hdr_t*)packet);
#endif

	switch(type){
	case(ethertype_ip):
			sr_handle_ip_packet(sr, (sr_ethernet_hdr_t*)packet, len, interface);
			break;
	case(ethertype_arp):
			sr_handle_arp_packet(sr, (sr_ethernet_hdr_t*)packet, len, interface);
			break;
	}
}/* end sr_ForwardPacket */

void sr_handle_ip_packet(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */)
{
	sr_ip_hdr_t *ip_packet = ip_hdr(packet);
	sr_if_t *inter = sr_get_interface_by_ip_addr(sr, ip_packet->ip_dst);
	sr_rt_t *entry;
	sr_arpentry_t *arp_entry;
	uint16_t checksum = ip_packet->ip_sum;
	uint16_t checksum_2;

	ip_packet->ip_sum = 0;
	checksum_2 = cksum(ip_packet, ip_packet->ip_hl * 4);
	ip_packet->ip_sum = checksum;

	if(ip_packet->ip_hl < 5 || checksum != checksum_2){ /* Invalid header size or corrupted */
		/* Drop packet */
		return;
	}

	if(inter){ /* IP packet addressed to router */
		sr_router_handle_ip_packet(sr, packet, len, interface);
	} else {
		entry = sr_find_max_prefix_matching(sr, ip_packet->ip_dst);

		ip_packet->ip_ttl--;

		if(ip_packet->ip_ttl == 0){ /* We should not forward a packet with time to live equal 0 */
			inter = sr_get_interface(sr, interface); /* send back */

			/* Return ip packet to its old state to copy into icmp packet */
			ip_packet->ip_ttl++;
			sr_send_icmp(sr, icmp_ttl_exceeded, icmp_ttl_expired, inter->ip, ip_packet->ip_src,
					(uint8_t*)ip_packet);
			return;
		}

		ip_packet->ip_sum = 0;
		ip_packet->ip_sum = cksum(ip_packet, ip_packet->ip_hl * 4);

		if(entry == NULL) { /* No entry found */
			inter = sr_get_interface(sr, interface); /* send back */

			sr_send_icmp(sr, icmp_destination_unreachable, icmp_network_unreachable, inter->ip, ip_packet->ip_src,
					(uint8_t*)ip_packet);
			return;
		}

		arp_entry = sr_arpcache_lookup(&sr->cache, entry->gw.s_addr);
		inter = sr_get_interface(sr, entry->interface);

		if(arp_entry){
			sr_forward_ip_packet(sr, packet, len, inter, arp_entry);

			free(arp_entry);
		} else {
			sr_arpcache_queuereq(sr, entry->gw.s_addr, (uint8_t*)packet, len, entry->interface);
		}
	}
}

void sr_router_handle_ip_packet(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */){
	sr_ip_hdr_t *ip_packet = ip_hdr(packet);
	sr_icmp_t3_hdr_t *icmp_packet;
	uint8_t  ether_dhost[ETHER_ADDR_LEN];
	uint8_t  ether_shost[ETHER_ADDR_LEN];
	uint16_t checksum;
	sr_rt_t *entry;
	sr_if_t *iface;
	uint16_t length;

	switch(ip_packet->ip_p){
	case ip_protocol_icmp:
		icmp_packet = icmp_hdr(packet);

		length = ntohs(ip_packet->ip_len) - ip_packet->ip_hl * 4;
		checksum = icmp_packet->icmp_sum;

		icmp_packet->icmp_sum = 0;
		icmp_packet->icmp_sum = cksum(icmp_packet, length);

		if(icmp_packet->icmp_sum != checksum) { /* Corrupted */
			/* Drop packet */
			DEBUG_MSG("sr_router_handle_ip_packet - icmp corrupted\n");
			return;
		}

		icmp_packet->icmp_type = icmp_echo_reply;

		swap_uint32(&ip_packet->ip_src, &ip_packet->ip_dst);

		memcpy(ether_dhost, packet->ether_shost, ETHER_ADDR_LEN);
		memcpy(ether_shost, packet->ether_dhost, ETHER_ADDR_LEN);

		sr_send_ethernet(sr, packet, len, htons(ethertype_ip), ether_shost, ether_dhost);
		break;
	case ip_protocol_tcp:
	case ip_protocol_udp:
		entry = sr_find_max_prefix_matching(sr, ip_packet->ip_src);
		iface = sr_get_interface(sr, entry->interface);

#if(DEBUG == TRUE)
		if(entry == NULL){
			DEBUG_MSG("ip forwarding not found %s", interface);
			exit(1);
		}
#endif

		sr_send_icmp(sr, icmp_destination_unreachable, icmp_port_unreachable,
				iface->ip, ip_packet->ip_src, (uint8_t*)ip_packet);

		break;
	default:
		/* Ignore packet */
		break;
	}

}

void sr_forward_ip_packet(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
		sr_if_t *interface,
		sr_arpentry_t *arp_entry)
{
	sr_send_ethernet(sr, packet, len, htons(ethertype_ip), interface->addr, arp_entry->mac);
}

void sr_handle_arp_packet(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */)
{
	sr_arp_hdr_t *arp_packet = arp_hdr(packet);
	uint16_t opcode = arp_opcode((uint8_t*)arp_packet);

	switch (opcode){
	case(arp_op_reply):
			sr_handle_arp_reply(sr, packet, len, interface);
			break;
	case(arp_op_request):
			sr_handle_arp_request(sr, packet, len, interface);
			break;
	}

}

void sr_handle_arp_request(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */){
	sr_arp_hdr_t *arp_packet = arp_hdr(packet);
	uint32_t ip_addr = arp_packet->ar_tip;
	sr_if_t *inter = sr_get_interface_by_ip_addr(sr, ip_addr);

	if(inter){ /* requesting one of our interfaces */
		sr_send_arp_reply(sr, packet, len, interface, inter->addr);
	} else {
		/* Drop packet */
	}
}

void sr_handle_arp_reply(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */){
	sr_ethernet_hdr_t *outgoing_packet;
	sr_arp_hdr_t *arp_packet = arp_hdr(packet);
	sr_arpreq_t *req = sr_arpcache_insert(&sr->cache, arp_packet->ar_sha, arp_packet->ar_sip);
	sr_packet_t *walk_packet;
	sr_if_t *iface;

	if(req){
		walk_packet = req->packets;

		while(walk_packet){
			outgoing_packet = (sr_ethernet_hdr_t*)walk_packet->buf;
			iface = sr_get_interface(sr, walk_packet->iface);

			sr_send_ethernet(sr, outgoing_packet, walk_packet->len, outgoing_packet->ether_type,
					iface->addr, arp_packet->ar_sha);

			walk_packet = walk_packet->next;
		}

		sr_arpreq_destroy(&sr->cache, req);
	}
}

int sr_send_arp_request(sr_instance_t *sr, sr_arpreq_t *req){
	sr_if_t *interface;
	sr_rt_t *entry;

	DEBUG_MSG("sr_send_arp_request\n");

	entry = sr_find_max_prefix_matching(sr, req->ip);

	if(entry == NULL){
		DEBUG_MSG("sr_send_arp_request ip address not found\n");
		return -1;
	}

	interface = sr_get_interface(sr, entry->interface);

	if(interface == NULL){
		DEBUG_MSG("sr_send_arp_request interface not found\n");
		return -1;
	}

	return sr_send_arp(sr, arp_op_request, interface->addr, interface->ip, NULL, req->ip);
}

void sr_send_arp_reply(sr_instance_t *sr,
		sr_ethernet_hdr_t *packet /* lent */,
        unsigned int len,
        char* interface/* lent */,
		unsigned char addr[ETHER_ADDR_LEN]){
	sr_arp_hdr_t *arp_packet = arp_hdr(packet);
	uint32_t tmp = arp_packet->ar_tip;

	arp_packet->ar_op = htons(arp_op_reply);
	arp_packet->ar_tip = arp_packet->ar_sip;
	arp_packet->ar_sip = tmp;
	memcpy((void*)arp_packet->ar_tha, (void*)arp_packet->ar_sha, ETHER_ADDR_LEN);
	memcpy((void*)arp_packet->ar_sha, (void*)addr, ETHER_ADDR_LEN);

	sr_send_ethernet(sr, packet, len, htons(ethertype_arp), arp_packet->ar_sha, arp_packet->ar_tha);
}

int sr_send_arp(sr_instance_t *sr, sr_arp_opcode_t opcode,
		uint8_t sha[ETHER_ADDR_LEN], uint32_t s_ip,
		uint8_t tha[ETHER_ADDR_LEN], uint32_t t_ip){
	sr_ethernet_hdr_t *packet = (sr_ethernet_hdr_t*)malloc(ARP_PACKET_SIZE);
	sr_arp_hdr_t *arp_packet = arp_hdr(packet);

	memset(packet, 0, ARP_PACKET_SIZE);
	arp_packet->ar_hrd = htons(ARP_FORMAT_HADWARE_ADDRESS);
	arp_packet->ar_pro = htons(ARP_FORMAT_PROTOCOL_ADDRESS);
	arp_packet->ar_hln = ARP_LENGTH_HARDWARE_ADDRESS;
	arp_packet->ar_pln = ARP_LENGTH_PROTOCOL_ADDRESS;
	arp_packet->ar_op = htons(opcode);
	memcpy(arp_packet->ar_sha, sha, ETHER_ADDR_LEN);
	arp_packet->ar_sip = s_ip;
	arp_packet->ar_tip = t_ip;

	if(tha != NULL)
		memcpy(arp_packet->ar_tha, tha, ETHER_ADDR_LEN);

	sr_send_ethernet(sr, packet, ARP_PACKET_SIZE, htons(ethertype_arp), sha, (uint8_t*)IP_BROADCAST);
	free(packet);

	return 0;
}

int sr_send_icmp(sr_instance_t *sr, sr_icmp_type_t type, sr_icmp_opcode_t opcode,
		uint32_t s_ip_addr, uint32_t d_ip_addr, uint8_t *data){
	sr_ethernet_hdr_t *packet = (sr_ethernet_hdr_t*)malloc(ICMP_PACKET_SIZE);
	sr_icmp_t3_hdr_t *icmp_packet = icmp_hdr(packet);

#if(DEBUG == TRUE)
	char src[32], dst[32];

	ip2str(src, s_ip_addr);
	ip2str(dst, d_ip_addr);
	DEBUG_MSG("sr_send_icmp src: %s dst: %s\n", src, dst);
#endif

	memset(packet, 0, ICMP_PACKET_SIZE);

	icmp_packet->icmp_type = type;
	icmp_packet->icmp_code = opcode;

	if(data)
		memcpy(icmp_packet->data, data, ICMP_DATA_SIZE);

	icmp_packet->icmp_sum = cksum((uint8_t*)icmp_packet, ICMP_HDR_SIZE);

	sr_send_ip(sr, packet, ICMP_PACKET_SIZE, ICMP_HDR_SIZE, ip_protocol_icmp, s_ip_addr, d_ip_addr);
	free(packet);

	return 0;
}

int sr_send_ip(sr_instance_t *sr, sr_ethernet_hdr_t *packet, uint32_t len,
		uint16_t upper_layer_len, uint8_t protocol, uint32_t s_ip_addr, uint32_t d_ip_addr){
	uint8_t packet_lent = 1;
	sr_ip_hdr_t *ip_packet = ip_hdr(packet);
	sr_rt_t *entry = sr_find_max_prefix_matching(sr, d_ip_addr);
	sr_if_t *interface = sr_get_interface(sr, entry->interface);
	sr_arpentry_t *arp_entry = sr_arpcache_lookup(&sr->cache, entry->gw.s_addr);

	if(interface == NULL){
		DEBUG_MSG("sr_send_ip - invalid interface (ip:%s)\n", ipv42str(s_ip_addr));
		return -1;
	}

	DEBUG_MSG("sr_send_ip - interface %s mac: %s ip: %s\n", interface->name, mac2str(interface->addr), ipv42str(s_ip_addr));

	if(packet == NULL){
		packet = (sr_ethernet_hdr_t*)malloc(IP_PACKET_SIZE);
		memset(packet, 0, IP_PACKET_SIZE);
		len = IP_PACKET_SIZE;
		packet_lent = 0;
	}

	ip_packet->ip_hl = IP_HDR_SIZE / 4;
	ip_packet->ip_v = IP_VERSION;
	ip_packet->ip_off |= IP_DF;
	ip_packet->ip_tos = 0;
	ip_packet->ip_len = htons(upper_layer_len + IP_HDR_SIZE);
	ip_packet->ip_id = 0;
	ip_packet->ip_off = 0;
	ip_packet->ip_ttl = INIT_TTL;
	ip_packet->ip_p = protocol;
	ip_packet->ip_src = s_ip_addr;
	ip_packet->ip_dst = d_ip_addr;
	ip_packet->ip_sum = cksum((uint8_t*)ip_packet, ip_packet->ip_hl * 4);

	if(arp_entry == NULL){
		packet->ether_type = htons(ethertype_ip);

		sr_arpcache_queuereq(sr, entry->gw.s_addr, (uint8_t*)packet, len, entry->interface);
		return -1;
	}

	sr_send_ethernet(sr, packet, len, htons(ethertype_ip), interface->addr, arp_entry->mac);

	if(packet_lent == 0)
		free(packet);
	free(arp_entry);
	return 0;
}

int sr_send_ethernet(sr_instance_t *sr, sr_ethernet_hdr_t *packet, uint32_t len, sr_ethertype_t type, uint8_t ether_shost[ETHER_ADDR_LEN], uint8_t ether_dhost[ETHER_ADDR_LEN])
{
	uint8_t packet_lent = 1;
	sr_if_t *interface = sr_get_interface_by_mac_addr(sr, ether_shost);

	if(interface == NULL){
		printf("sr_send_ethernet - invalid interface mac:%s\n", mac2str(ether_shost));
		return -1;
	}

	if(packet == NULL){
		packet = (sr_ethernet_hdr_t*)malloc(ETHER_HDR_SIZE);
		memset(packet, 0, ETHER_HDR_SIZE);
		len = ETHER_HDR_SIZE;
		packet_lent = 0;
	}

	memcpy((void*)packet->ether_shost, (void*)ether_shost, ETHER_ADDR_LEN);
	memcpy((void*)packet->ether_dhost, (void*)ether_dhost, ETHER_ADDR_LEN);
	packet->ether_type = type;

#if (NAT == TRUE)
	sr_nat_map_outgoing_packet(sr, packet);
#endif

	sr_send_packet(sr, (uint8_t*)packet, len, interface->name);
	if(packet_lent == 0)
		free(packet);
	return 0;
}


